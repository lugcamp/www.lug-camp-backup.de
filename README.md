# Lug Camp Backup

Das Projekt "LUG Camp Backup" in dieser Form auf dem LUG Camp 2019 in Ullerup (DK) entstanden. Wir pflegen hier auf Basis von [Git](https://git-scm.de) und [Nikola](https://getnikola.com) die Webseite auf der wir Erinnerungen und Geschichten rund um das LUG Camp veröffentlichen möchten.

## Mitarbeit an diesem Projekt

Wenn Ihr Artikel hinzufügen möchtet, beantragt bitte den Beitritt in die Gruppe "lugcamp" auf gitlab.com. Im Anschluß könnt Ihr in diesem Repository arbeiten. Ihr erhaltet jedoch auf den Haupt-Branch "master" keinen Zuriff, da dieser automatisch veröffentlicht wird. Änderungen können nur mit einem Mergerequests in den Master überführt werden.

Mehr Infos und Unterstützung könnt Ihr bei Frank Agerholm (LUG Flensburg e.V.) bekommen.
