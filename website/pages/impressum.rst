.. title: Impressum
.. slug: impressum
.. date: 2019-06-01 20:17:55 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Inhaltlich Verantwortlicher gemäß §10 Absatz 3 MDStV
----------------------------------------------------

| Frank Agerholm
| Linux User Group Flensburg e.V.
| Roggenbogen 14
| 24943 Flensburg


EMail : frank(at)lugfl.de.de



Haftungshinweis
---------------

Trotz sorgfältiger inhaltlicher Kontrolle übernehmen ich keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschliesslich deren Betreiber verantwortlich.

